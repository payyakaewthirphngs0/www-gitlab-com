---
layout: handbook-page-toc
title: "CI Adoption Landing Zone"
description: "A page containing links to helpful resources for the CSM team and our customers"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Migrating to GitLab
